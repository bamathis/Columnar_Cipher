# Columnar_Cipher

[Columnar_Cipher](https://gitlab.com/bamathis/Columnar_Cipher) is an implemention of the Columnar cipher for encryption and decryption

## Quick Start

### Program Execution

#### Encrypting/Decrypting
```
$ ./ColumnarCipher.cpp 
```

### Known Issues

- The given input file must only consist of letters and spaces. 