//Name:Brandon Mathis
//File Name:ColumnarCipher.cpp
//Date:March 10, 2016
//Program Description:Encryption/Decryption using Columnar Cipher

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <iomanip>
#include <math.h>

using namespace std;
int getPlainText(ifstream& input, string& plainText);
int getCipherText(ifstream& input, string& cipherText);
void encrypt(string plainText,int& length,string keyword,int keyWordLength,string& cipherText);
void decrypt(string& plainText,int& length,string keyword,int keyWordLength,string cipherText);
void writeText(string text,ostream& output, int textLength);

int main ()
	{
	char choice;
	ifstream input;
	string plainText;
	string cipherText;
	string inFileName;
	ofstream outFile;
	string outFileName;
	string keyword;
	int length;
	int keyWordLength;
	cout << "Encrypt or Decrypt(E/D)? ";
	cin >> choice;
	cin.ignore(999,'\n');
	if(islower(choice))
		choice = choice + 'A' - 'a';

	cout << "Enter name for source file: ";
	getline(cin,inFileName);
	input.open(inFileName.c_str());
	if(input.fail())
		{
		cout << "File not found" << endl;
		exit(1);
		}
	cout << "Enter name for destination file: ";
	getline(cin,outFileName);
	cout << "Enter keyword for encryption/decryption: ";
	getline(cin,keyword);
	for(keyWordLength = 0; keyword[keyWordLength] != '\0'; keyWordLength++)
		{
		if(isupper(keyword[keyWordLength]))			//Converts keyword to lowercase
			keyword[keyWordLength] = keyword[keyWordLength] - 'A' + 'a';
		}
	if(choice == 'E')
		{
		length = getPlainText(input, plainText);
		input.close();
		encrypt(plainText,length,keyword,keyWordLength,cipherText);
		outFile.open(outFileName.c_str());
		writeText(cipherText,cout,length);
		writeText(cipherText,outFile,length);
		outFile.close();
		}
	else if(choice == 'D')
		{
		length = getCipherText(input, cipherText);
		input.close();
		decrypt(plainText,length,keyword,keyWordLength,cipherText);
		outFile.open(outFileName.c_str());
		writeText(plainText,cout,length);
		writeText(cipherText,outFile,length);
		outFile.close();
		}
	}
//=====================================================
//Reads input file into plainText string
int getPlainText(ifstream& input, string& plainText)
	{
	int length;
	char temp;
	plainText = "";
	length = 0;
	while(input.get(temp))
		{
		if(isupper(temp))					//Converts text to lowercase
			temp = temp - 'A' + 'a';
		if(temp >= 'a' && temp <= 'z')
			{
			plainText = plainText + temp;
			length++;
			}
		}
	plainText = plainText + '\0';
	return length;
	}
//=====================================================
//Reads input file into cipherText string
int getCipherText(ifstream& input, string& cipherText)
	{
	int length;
	char temp;
	cipherText = "";
	length = 0;
	while(input.get(temp))
		{
		if(islower(temp))					//Converts text to uppercase
			temp = temp + 'A' - 'a';
		if(temp >= 'A' && temp <= 'Z')
			{
			cipherText = cipherText + temp;
			length++;
			}
		}
	cipherText = cipherText + '\0';
	return length;
	}
//=====================================================
//Encrypts given plain text using given keyword
void encrypt(string plainText,int& length,string keyword,int keyWordLength,string& cipherText)
	{
	int cipher;
	int rows = ceil((float)length/keyWordLength);
	char text[rows][keyWordLength] = {0};
	int positionCount[keyWordLength] = {0};
	int positionOrder[keyWordLength];
	int currentPosition = 0;
	cipherText = "";
	for(int n = 0; n < rows; n++)				//Stores the text from plaintext into a block
		for(int m = 0; m < keyWordLength; m++)
			text[n][m] = plainText[n * keyWordLength  + m];

	for(int n = 0; n < keyWordLength; n++)		//Finds the order of the keyword characters
		for(int m = 0; m < keyWordLength; m++)
			if(keyword[n] > keyword[m] && n!=m)
				positionCount[n]++;

	for(int n = 0; n < keyWordLength; n++)
		for(int m = 0; m < keyWordLength; m++)
			{
			if(positionCount[m] == n)			//Stores the columns in the order in which they need to be added to ciphertext
				{
				positionOrder[currentPosition] = m;
				positionCount[m] = -1;
				currentPosition++;
				}
			}
	for(int n = 0; n < keyWordLength; n++)
		for(int m = 0; m < rows; m++)
			{
			if(!isupper(text[m][positionOrder[n]]))	//Converts the text to uppercase
				text[m][positionOrder[n]] = text[m][positionOrder[n]] - 'a' + 'A';
			if(text[m][positionOrder[n]] <= 'Z' && text[m][positionOrder[n]] >= 'A')
				cipherText = cipherText + text[m][positionOrder[n]];
			}

	}
//=====================================================
//decrypts given plain text using given keyword
void decrypt(string& plainText,int& length,string keyword,int keyWordLength,string cipherText)
	{
	int cipher;
	int rows = ceil((float)length/keyWordLength);
	int remainder = length % keyWordLength;
	char text[rows][keyWordLength] = {0};
	int positionCount[keyWordLength] = {0};
	int positionOrder[keyWordLength];
	int outputOrder[keyWordLength];
	int currentPosition = 0;
	plainText = "";

	for(int n = 0; n < keyWordLength; n++)			//Finds the order of the keyword characters
		for(int m = 0; m < keyWordLength; m++)
			if(keyword[n] > keyword[m] && n!=m)
				positionCount[n]++;

	for(int n = 0; n < keyWordLength; n++)
		for(int m = 0; m < keyWordLength; m++)
			{
			if(positionCount[m] == n)
				{
				positionOrder[currentPosition] = m;	//Used to find columns that won't be full
				outputOrder[m] = currentPosition;	//Stores the columns in the order in which they need to be added to plaintext
				positionCount[m] = -1;
				currentPosition++;
				}
			}
	currentPosition = 0;
	for(int n = 0; n < keyWordLength; n++)				//Stores the text from ciphertext into a block
		for(int m = 0; m < rows; m++)
		{
			if(m == rows-1 && positionOrder[n] >= remainder && remainder != 0)
				text[m][n] = '\0';
			else
				text[m][n] = cipherText[currentPosition++];
			}

	for(int n = 0; n < rows; n++)
		{
		for(int m = 0; m < keyWordLength; m++)
			{
			if(!islower(text[n][outputOrder[m]]))		//Converts the text to lowercase
				text[n][outputOrder[m]] = text[n][outputOrder[m]] - 'A' + 'a';
			if(text[n][outputOrder[m]] <= 'z' && text[n][outputOrder[m]] >= 'a')
				plainText = plainText + text[n][outputOrder[m]];
			}
		}

	}
//=====================================================
//Displays the text on screen or writes to file
void writeText(string text,ostream& output, int textLength)
	{
	output << endl;
	for(int n = 0; n < textLength; n++)
		{
		output << text[n];
		if((n+1) % 5 == 0)
			output << " ";
		if((n+1)%20 == 0)
			output << endl;
		if((n+1) % 100 == 0)
			output << endl;
		}
	output << endl;
	}